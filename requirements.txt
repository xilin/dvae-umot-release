numpy==1.22.0

Pillow

scikit-learn
pandas
scipy
SoundFile
librosa
matplotlib

tensorboard

torch
torchaudio
torchvision

motmetrics
