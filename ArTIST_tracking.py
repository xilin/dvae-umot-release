## DVAE-UMOT
## Copyright Inria
## Year 2022
## Contact : xiaoyu.lin@inria.fr

## DVAE-UMOT is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.

## DVAE-UMOT is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program, DVAE-UMOT.  If not, see <http://www.gnu.org/licenses/> and the LICENSE file.

# DVAE-UMOT has code derived from 
# (1) ArTIST, https://github.com/fatemeh-slh/ArTIST.
# (2) DVAE, https://github.com/XiaoyuBIE1994/DVAE, distributed under MIT License 2020 INRIA.

from models.ArTIST.utils.utils_ar import iou
from models.ArTIST.utils.utils_ar import infer_log_likelihood, iou
import numpy as np
import torch
import motmetrics as mm
from configparser import ConfigParser
import sys
import os
from scipy.optimize import linear_sum_assignment
from models.ArTIST.utils.clustering import load_clusters
from models.ArTIST.ar import motion_ar
from models.ArTIST.ae import motion_ae
from scipy.optimize import linear_sum_assignment
from data.data_loader import create_dataloader
import pickle
import datetime

torch.backends.cudnn.enabled = False

def tracking_framet(t_frame, max_id, t_trs, detections_future, tracklet_list, n_sampling, centroid_x, centroid_y, centroid_w, centroid_h, model_ar, num_cluster):
    # t_frame: int, current frame
    # max_id: int, max assigned id
    # detection_future3: (t_trs+1, num_det, 4), list of detections of current frame and future t_trs frames
    # tracklet_list: (num_tracklet), list of tracklet dictionaries

    detection_current_frame = detections_future[0, :, :]
    detection_current_frame = detection_current_frame[~torch.any(detection_current_frame.isnan(),dim=1)]
    num_det = detection_current_frame.shape[0]
    num_tracklet = len(tracklet_list)

    cost_matrix = torch.zeros(num_tracklet, num_det).cuda()
    full_seq_idx = []
    inpaint_seq_idx = []
    inpaint_seq_list = []

    gaussian_kernel = np.load("data/ArTIST_data/kernel.npy")
    gaussian_kernel = torch.autograd.Variable(torch.from_numpy(gaussian_kernel).float()).cuda()
    gaussian_kernel = gaussian_kernel.unsqueeze(0).unsqueeze(0)

    for i in range(num_tracklet):
        sequence = tracklet_list[i]['sequence']
        gap = tracklet_list[i]['gap']
        start = tracklet_list[i]['start']
        end = tracklet_list[i]['end']
        social = tracklet_list[i]['social'][:, start:end+gap+t_trs+1, :]
        tracklet_id = tracklet_list[i]['ID']

        # computing the motion velocity
        if sequence.shape[1] == 1:
            tracklet_delta = torch.zeros(sequence.shape[0], sequence.shape[1],
                                 sequence.shape[2]).cuda()
        else:
            tracklet_delta_tmp = sequence[:, 1:, :] - sequence[:, 0:-1, :]
            tracklet_delta = torch.zeros(tracklet_delta_tmp.shape[0], tracklet_delta_tmp.shape[1] + 1,
                                     tracklet_delta_tmp.shape[2]).cuda()
            tracklet_delta[:, 1:, :] = tracklet_delta_tmp

        # Inpainting and compute the cost matrix
        if gap == 0:
            full_seq_idx.append(i)
            y_g1d_x, y_g1d_y, y_g1d_w, y_g1d_h, sampled_boxes, sampled_deltas, sampled_detection = generate_distrib(sequence, social, gap, model_ar, centroid_x,
        centroid_y,
        centroid_w,
        centroid_h, gaussian_kernel, num_cluster)

            distribs = [y_g1d_x, y_g1d_y, y_g1d_w, y_g1d_h]
            centroids = [centroid_x, centroid_y, centroid_w, centroid_h]
            last_bbox = sequence[0, -1, :]
            scores = scoring(sampled_boxes, last_bbox, tracklet_delta, sampled_deltas, detection_current_frame, distribs, centroids, num_cluster)
            inpaint_seq_list.append(sampled_boxes)

        elif gap > 0:
            t_trs = 2
            with torch.no_grad():
                dist_x, dist_y, dist_w, dist_h, sampled_boxes, sampled_deltas, sampled_detection = model_ar.batch_inference(
                    sequence.repeat(n_sampling, 1, 1), social.repeat(n_sampling, 1, 1), gap + t_trs + 1, centroid_x, centroid_y,
                    centroid_w, centroid_h)

            centroids = [centroid_x, centroid_y, centroid_w, centroid_h]
            best_inpainting, best_detection, best_deltas, best_dist_x, best_dist_y, best_dist_w, best_dist_h = trs(sampled_boxes, sampled_detection, sampled_deltas, dist_x, dist_y, dist_w, dist_h, detections_future, t_trs)

            if len(best_inpainting) != 0:
                inpaint_seq_idx.append(i)
                # making it a probability distribution
                dist_x = torch.nn.Softmax(dim=-1)(best_dist_x).permute(1, 0, 2)
                dist_y = torch.nn.Softmax(dim=-1)(best_dist_x).permute(1, 0, 2)
                dist_w = torch.nn.Softmax(dim=-1)(best_dist_w).permute(1, 0, 2)
                dist_h = torch.nn.Softmax(dim=-1)(best_dist_h).permute(1, 0, 2)

                # smoothing the distributions using a gaussian kernel
                y_g1d_x = torch.nn.functional.conv1d(dist_x, gaussian_kernel,
                                                     padding=24).permute(1, 0, 2)
                y_g1d_y = torch.nn.functional.conv1d(dist_y, gaussian_kernel,
                                                     padding=24).permute(1, 0, 2)
                y_g1d_w = torch.nn.functional.conv1d(dist_w, gaussian_kernel,
                                                     padding=24).permute(1, 0, 2)
                y_g1d_h = torch.nn.functional.conv1d(dist_h, gaussian_kernel,
                                                     padding=24).permute(1, 0, 2)
                distribs = [y_g1d_x, y_g1d_y, y_g1d_w, y_g1d_h]
                last_bbox = best_inpainting[0, -1, :]
                scores = scoring(best_inpainting, last_bbox, tracklet_delta, best_deltas, detection_current_frame, distribs, centroids,
                        num_cluster)
            else:
                scores = -1000 * torch.ones(num_det).cuda()
            inpaint_seq_list.append(best_inpainting)

        cost_matrix[i, :] = scores

    detection_idx = np.arange(num_det)
    # Assignment for full sequences
    assignment_fullseq, uD_fullseq, uT_fullseq = Munkres(cost_matrix, detection_idx, full_seq_idx)

    # Assignment for inpainted sequences
    assignment_inpaint, uD_inpaint, uT_inpaint = [], uD_fullseq, inpaint_seq_idx
    if (uD_fullseq.size != 0) & (np.sum([len(inpaint_seq_list[j]) for j in range(len(inpaint_seq_list))]) != 0):
        assignment_inpaint, uD_inpaint, uT_inpaint = Munkres(cost_matrix, uD_fullseq, inpaint_seq_idx)

    # Update existing tracklets:
    # For assigned fullseq tracklets
    if len(assignment_fullseq) != 0:
        for ass in assignment_fullseq:
            tracklet_idx = ass[0]
            det_idx = ass[1]
            detection = detection_current_frame[det_idx].unsqueeze(0).unsqueeze(0)
            tracklet_list[tracklet_idx]['sequence'] = torch.cat((tracklet_list[tracklet_idx]['sequence'], detection), 1)
            tracklet_list[tracklet_idx]['end'] = t_frame + 1
    # For unassigned tracklets
    if len(uT_fullseq) != 0:
        for uT_idx in uT_fullseq:
            tracklet_list[uT_idx]['gap'] += 1
    # For assigned inpainted tracklets
    if len(assignment_inpaint) != 0:
        for ass in assignment_inpaint:
            tracklet_idx = ass[0]
            det_idx = ass[1]
            detection = detection_current_frame[det_idx].unsqueeze(0).unsqueeze(0)
            inpainted_seq = inpaint_seq_list[ass[0]]
            tracklet_list[tracklet_idx]['sequence'] = torch.cat((tracklet_list[tracklet_idx]['sequence'], inpainted_seq, detection), 1)
            tracklet_list[tracklet_idx]['gap'] = 0
            tracklet_list[tracklet_idx]['end'] = t_frame + 1
    # For unassigned inpainted tracklets
    if len(uT_inpaint) != 0:
        for unassigned_idx in uT_inpaint:
            tracklet_list[unassigned_idx]['gap'] += 1
    # Generate new tracklets
    # if len(uD_inpaint) != 0:
    #     for uD_idx in uD_inpaint:
    #         new_tracklet = {}
    #         new_tracklet['sequence'] = detection_current_frame[uD_idx].unsqueeze(0).unsqueeze(0)
    #         new_tracklet['gap'] = 0
    #         new_tracklet['start'] = t_frame
    #         new_tracklet['end'] = t_frame + 1
    #         new_tracklet['ID'] = max_id
    #         new_tracklet['social'] = social_list[max_id].unsqueeze(0).cuda()
    #         tracklet_list.append(new_tracklet)
    #         max_id += 1

    return tracklet_list, max_id

def generate_distrib(sequence, social, gap, model_ar, centroid_x,
        centroid_y,
        centroid_w,
        centroid_h, gaussian_kernel, num_cluster):

    # sequence: (1, track_len, 4)
    # social: (1, track_len, 256)

    # computing the distribution over the next plausible bounding box
    dist_x, dist_y, dist_w, dist_h, sampled_boxes, sampled_deltas, sampled_detection = model_ar.inference(
        sequence,
        social,
        gap,
        centroid_x,
        centroid_y,
        centroid_w,
        centroid_h)

    # making it a probability distribution
    dist_x = torch.nn.Softmax(dim=-1)(dist_x).permute(1, 0, 2)
    dist_y = torch.nn.Softmax(dim=-1)(dist_y).permute(1, 0, 2)
    dist_w = torch.nn.Softmax(dim=-1)(dist_w).permute(1, 0, 2)
    dist_h = torch.nn.Softmax(dim=-1)(dist_h).permute(1, 0, 2)

    # smoothing the distributions using a gaussian kernel
    y_g1d_x = torch.nn.functional.conv1d(dist_x, gaussian_kernel,
                                         padding=24).permute(1,0,2)
    y_g1d_y = torch.nn.functional.conv1d(dist_y, gaussian_kernel,
                                         padding=24).permute(1,0,2)
    y_g1d_w = torch.nn.functional.conv1d(dist_w, gaussian_kernel,
                                         padding=24).permute(1,0,2)
    y_g1d_h = torch.nn.functional.conv1d(dist_h, gaussian_kernel,
                                         padding=24).permute(1,0,2)

    return y_g1d_x, y_g1d_y, y_g1d_w, y_g1d_h, sampled_boxes, sampled_deltas, sampled_detection

def scoring(sampled_boxes, last_bbox, tracklet_delta, sampled_delta, true_detections, distribs, centroids, num_cluster):
    # tracklet_delta: (1, track_len, 4)
    # true_dections: (num_det, 4)
    # sampled_boxes: (1, gap+t_trs-1, 4)
    # last_bbox: (1, 1, 4)

    # scores: (num_det)

    num_det = true_detections.shape[0]
    if len(sampled_boxes) == 0:
        extended_track = torch.zeros(1, tracklet_delta.shape[1] + 1, 4).cuda()
    else:
        extended_track = torch.zeros(1, tracklet_delta.shape[1] + sampled_delta.shape[1] + 1, 4).cuda()
    extended_track[0, :tracklet_delta.shape[1], :] = tracklet_delta[0, :, :]
    extended_track[0, tracklet_delta.shape[1]:-1, :] = sampled_delta

    scores = torch.zeros(num_det).cuda()
    for i in range(num_det):
        last_delta = true_detections[i, :] - last_bbox.detach()
        extended_track[0, -1, :] = last_delta
        likelihoods_smooth = infer_log_likelihood(distribs[0], distribs[1], distribs[2], distribs[3],
                                                  extended_track[:, 1:, 0:1],
                                                  extended_track[:, 1:, 1:2],
                                                  extended_track[:, 1:, 2:3],
                                                  extended_track[:, 1:, 3:4],
                                                  centroids[0], centroids[1], centroids[2], centroids[3], num_cluster)
        all_scores = np.array(likelihoods_smooth[-1])
        likelihoods_smooth = np.sum(all_scores)
        score = torch.tensor(likelihoods_smooth.astype(np.float64))
        scores[i] = score

    return scores

def trs(sampled_boxes, sampled_detection, sampled_deltas, dist_x, dist_y, dist_w, dist_h, true_detections, t_trs):

    # sampled_boxes: (n_sample, gap+t_trs, 4)
    # sampled_detection: (n_sample, 1, 4)
    # sampled_deltas: (n_sample, gap+t_trs, 4)
    # dist_x: (n_sample, seq_len-1+gap+t_trs, 1024)
    # true_dections: (t_trs+1, num_det, 4)

    n_sampling = sampled_boxes.shape[0]
    detection_len = true_detections.shape[0]
    num_det = true_detections.shape[1]
    t_frames = sampled_boxes.shape[1]
    dims = sampled_boxes.shape[2]

    iou_list = []
    inpainted_boxes = torch.cat((sampled_boxes, sampled_detection), 1)

    for n in range(n_sampling):
        current_inpaint_box = inpainted_boxes[n, -t_trs-1, :].cpu().detach().numpy()
        max_iou_n = 0
        for i in range(num_det):
            if torch.isnan(true_detections[0, i, 0]):
                continue
            else:
                current_det_box = true_detections[0, i, :].cpu().detach().numpy()
                current_iou = mm.distances.boxiou(current_inpaint_box, current_det_box)
                if current_iou > 0.1:
                    sum_iou = 0
                    for k in range(detection_len):
                        iou_k = mm.distances.boxiou(inpainted_boxes[n, k-t_trs-1, :].cpu().detach().numpy(), true_detections[k, i, :].cpu().detach().numpy())
                        if iou_k > 0.1:
                            sum_iou += iou_k
                    if sum_iou > max_iou_n:
                        max_iou_n = sum_iou
        iou_list.append(max_iou_n)
    if np.max(iou_list) > 0:
        max_iou_idx = np.argmax(iou_list)
        best_inpainting = sampled_boxes[max_iou_idx, :-t_trs, :].unsqueeze(0)
        best_detection = sampled_boxes[max_iou_idx, -t_trs:-t_trs+1, :].unsqueeze(0)
        best_deltas = sampled_deltas[max_iou_idx, :-t_trs, :].unsqueeze(0)
        best_dist_x = dist_x[max_iou_idx, :-t_trs-1, :].unsqueeze(0)
        best_dist_y = dist_y[max_iou_idx, :-t_trs-1, :].unsqueeze(0)
        best_dist_w = dist_w[max_iou_idx, :-t_trs-1, :].unsqueeze(0)
        best_dist_h = dist_h[max_iou_idx, :-t_trs-1, :].unsqueeze(0)
    elif np.max(iou_list) == 0:
        best_inpainting = []
        best_detection = []
        best_deltas = []
        best_dist_x = []
        best_dist_y = []
        best_dist_w = []
        best_dist_h = []

    return best_inpainting, best_detection, best_deltas, best_dist_x, best_dist_y, best_dist_w, best_dist_h

def Munkres(cost_matrix, detection_idx, tracklet_idx):
    cost_seq = cost_matrix[tracklet_idx, :][:, detection_idx].cpu().numpy()
    row_ind, col_ind = linear_sum_assignment(-cost_seq)
    assigned_T_idx = []
    assigned_D_idx = []
    for idx_T in row_ind:
        assigned_T_idx.append(tracklet_idx[idx_T])
    for idx_D in col_ind:
        assigned_D_idx.append(detection_idx[idx_D])

    uT = np.delete(tracklet_idx, row_ind)
    uD = np.delete(detection_idx, col_ind)

    assignment = list(zip(assigned_T_idx, assigned_D_idx))

    return assignment, uD, uT

def tracking(all_detections, all_social, t_trs, n_sampling, centroid_x, centroid_y, centroid_w, centroid_h, model_ar, num_cluster):
    # all_detections: (seq_len, num_obs, x_dim=4)
    # all_social: (num_obs, seq_len, dim_social=256)
    # Initialize parameters
    seq_len = all_detections.shape[0]
    num_obs = all_detections.shape[1]
    dim_social = all_social.shape[-1]

    # Initialize the tracklets using the first frame detections
    max_id = 0
    tracklet_list = []
    detection_frame0 = all_detections[0:5, :, :]
    used_social_idx = []
    for k in range(num_obs):
        if torch.isnan(detection_frame0[0, k, 0]):
            continue
        else:
            new_tracklet = {}
            new_tracklet['sequence'] = detection_frame0[:, k].unsqueeze(0).cuda()
            new_tracklet['gap'] = 0
            new_tracklet['start'] = 0
            new_tracklet['end'] = 5
            new_tracklet['social'] = all_social[k].unsqueeze(0).cuda()
            new_tracklet['ID'] = max_id
            tracklet_list.append(new_tracklet)
            used_social_idx.append(k)
            max_id += 1
    # Tracking by iterating each frame
    for t_frame in range(5, seq_len):
        detections_future = all_detections[t_frame:t_frame+t_trs+1, :, :]
        tracklet_list, max_id = tracking_framet(t_frame, max_id, t_trs, detections_future, tracklet_list, n_sampling, centroid_x, centroid_y, centroid_w, centroid_h, model_ar, num_cluster)

    return tracklet_list

if __name__ == '__main__':
    if len(sys.argv) == 2:
        cfg_file = sys.argv[1]
    else:
        print('Error: Please indicate config file path')

    if not os.path.isfile(cfg_file):
        raise ValueError('Invalid config file path')
    cfg = ConfigParser()
    cfg.read(cfg_file)

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    # Load in the models
    model_ae = motion_ae(256).cuda()
    model_ae.load_state_dict(torch.load('models/ArTIST/ae/ae_8.pth'))
    model_ae.eval()
    model_ar = motion_ar(512, 1024).cuda()
    model_ar.load_state_dict(torch.load('models/ArTIST/ar/ar_110.pth'))
    model_ar.eval()
    centroid_x, centroid_y, centroid_w, centroid_h = load_clusters()
    num_cluster = 1024
    n_sampling = 50
    t_trs = 2

    # Load in the data
    tracking_data_loader, tracking_data_size = create_dataloader(cfg, data_type='dvaeumot')
    gt_list = []
    obs_list = []
    tracking_list = []
    for idx, data in enumerate(tracking_data_loader):
        start_time = datetime.datetime.now()
        print('batch {}\n'.format(idx))
        gt_data = data['gt'][0].to(device).float()
        all_detections = data['det'][0].to(device).float()
        all_social = data['social'][0].to(device).float()
        tracking_res = tracking(all_detections, all_social, t_trs, n_sampling, centroid_x, centroid_y, centroid_w, centroid_h, model_ar, num_cluster)
        gt_list.append(gt_data.to('cpu'))
        obs_list.append(all_detections.to('cpu'))
        tracking_res_list = []
        for i in range(3):
            tracking_res_list.append(tracking_res[i]['sequence'].to('cpu'))
        tracking_list.append(tracking_res_list)
        t = (datetime.datetime.now() - start_time).seconds / 60
        print("time: {}".format(t))

    save_path = "results/artist/short_seq"
    save_path_tracking = os.path.join(save_path, "tracking_res.pkl")
    save_path_gt = os.path.join(save_path, "tracking_gt.pkl")
    save_path_obs = os.path.join(save_path, "tracking_obs.pkl")

    if not(os.path.isdir(save_path)):
        os.makedirs(save_path)
    with open(save_path_tracking, 'wb') as file:
        pickle.dump(tracking_list, file)
    with open(save_path_gt, 'wb') as file:
        pickle.dump(gt_list, file)
    with open(save_path_obs, 'wb') as file:
        pickle.dump(obs_list, file)
