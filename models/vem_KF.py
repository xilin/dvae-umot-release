## DVAE-UMOT
## Copyright Inria
## Year 2022
## Contact : xiaoyu.lin@inria.fr

## DVAE-UMOT is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.

## DVAE-UMOT is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program, DVAE-UMOT.  If not, see <http://www.gnu.org/licenses/> and the LICENSE file.

# DVAE-UMOT has code derived from 
# (1) ArTIST, https://github.com/fatemeh-slh/ArTIST.
# (2) DVAE, https://github.com/XiaoyuBIE1994/DVAE, distributed under MIT License 2020 INRIA.

import torch
import datetime

class VEM_KF_MODEL():
    def __init__(self, cfg, device, save_log):
        self.device = device

        self.N_iter = cfg.getint('VEM', 'N_iter')
        self.num_source = cfg.getint('VEM', 'num_source')
        self.num_obs = cfg.getint('VEM', 'num_obs')
        self.std_ratio = cfg.getfloat('VEM', 'std_ratio')
        self.seq_len = cfg.getint('DataFrame', 'sequence_len')

        self.o_dim = cfg.getint('VEM', 'o_dim')
        self.x_dim = cfg.getint('Network', 'x_dim')
        self.z_dim = cfg.getint('Network', 'z_dim')
        self.save_log = save_log

    def model_training(self, data_obs, data_gt, batch_idx, save_frequence):
        ### Tensor dimensions
        ## Initialized tensors
        # A: (o_dim, x_dim)
        # z_mean_inf_init: (num_source, batch_size, seq_len, z_dim)
        # z_logvar_inf_init: (num_source, batch_size, seq_len, z_dim)
        # x_mean_vem_init: (num_source, batch_size, seq_len, x_dim)
        # x_var_vem_init: (num_source, batch_size, seq_len, x_dim, x_dim)
        # Phi_init: (batch_size, seq_len, num_obs, o_dim, o_dim)

        ## Recording tensors
        # Eta_iter: (N_iter, num_source, batch_size, seq_len, num_obs)
        # z_mean_inf_iter: (N_iter, num_source, batch_size, seq_len, z_dim)
        # z_logvar_inf_iter: (N_iter, num_source, batch_size, seq_len, z_dim)
        # x_mean_vem_iter: (N_iter, num_source, batch_size, seq_len, x_dim)
        # x_var_vem_iter: (N_iter, num_source, batch_size, seq_len, x_dim, x_dim)
        # Phi_iter: (N_iter, batch_size, seq_len, num_obs, o_dim, o_dim)
        # Phi_inv_iter: (N_iter, batch_size, seq_len, num_obs, o_dim, o_dim)

        ## Tensors inside iteration
        # Eta_n: (batch_size, seq_len, num_obs)
        # z_mean_inf_n: (batch_size, seq_len, z_dim)
        # z_logvar_inf_n: (batch_size, seq_len, z_dim)
        # x_mean_vem_n: (batch_size, seq_len, x_dim)
        # x_var_vem_n: (batch_size, seq_len, x_dim, x_dim)
        # Phi: (batch_size, seq_len, num_obs, o_dim, o_dim)
        # Phi_inv: (batch_size, seq_len, num_obs, o_dim, o_dim)

        # data_obs: (batch_size, seq_len, num_obs, o_dim)

        # Parameters Initialization
        # Define the observation-state projection matrix as well as the state transition matrix
        A = torch.tensor([[1, 0, 0, 0, 0, 0], [0, 1, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0], [0, 0, 0, 1, 0, 0]]).float().to(self.device)
        D = torch.tensor([[1, 0, 0, 0, 1, 0], [0, 1, 0, 0, 0, 1], [0, 0, 1, 0, 0, 0], [0, 0, 0, 1, 0, 0], [0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 1]]).float().to(self.device)
        data_obs = data_obs.float()
        batch_size = data_obs.shape[0]

        # Initialize the variance matrix with the size of detection bounding boxes at the first frame
        variance_matrix = torch.zeros(batch_size, self.seq_len, self.num_obs, self.o_dim, self.o_dim).to(self.device)
        variance_matrix_inv = torch.zeros(batch_size, self.seq_len, self.num_obs, self.o_dim, self.o_dim).to(self.device)
        for i in range(batch_size):
            for j in range(self.num_obs):
                std_onesource = torch.zeros(self.o_dim)
                w = data_obs[i, 0, j, 2] - data_obs[i, 0, j, 0]
                h = data_obs[i, 0, j, 1] - data_obs[i, 0, j, 3]
                std_w = w * self.std_ratio
                std_h = h * self.std_ratio
                std_onesource[0] = std_w
                std_onesource[2] = std_w
                std_onesource[1] = std_h
                std_onesource[3] = std_h
                variance_matrix_onesource = torch.diag(torch.pow(std_onesource, 2))
                variance_matrix_onesource_inv = torch.inverse(variance_matrix_onesource)
                variance_matrix_onesource_seq = variance_matrix_onesource.expand(self.seq_len, self.o_dim, self.o_dim)
                variance_matrix_onesource_inv_seq = variance_matrix_onesource_inv.expand(self.seq_len, self.o_dim, self.o_dim)

                variance_matrix[i, :, j, :, :] = variance_matrix_onesource_seq
                variance_matrix_inv[i, :, j, :, :] = variance_matrix_onesource_inv_seq

        Phi = variance_matrix
        Phi_inv = variance_matrix_inv
        Lambda = torch.eye(self.x_dim).repeat(self.num_source, batch_size, self.seq_len, 1, 1)
        Lambda[:, :, :, :4, :4] = Phi.permute(2, 0, 1, 3, 4)

        Eta_iter = torch.zeros(self.N_iter, self.num_source, batch_size, self.seq_len, self.num_obs).to(self.device)
        x_mean_vem_iter = torch.zeros(self.N_iter, self.num_source, batch_size, self.seq_len, self.x_dim).to(self.device)
        x_var_vem_iter = torch.zeros(self.N_iter, self.num_source, batch_size, self.seq_len, self.x_dim, self.x_dim).to(self.device)
        Lambda_iter = torch.zeros(self.N_iter, self.num_source, batch_size, self.seq_len, self.x_dim, self.x_dim).to(self.device)
        x_mean_vem_init = torch.zeros((self.num_source, batch_size, self.seq_len, self.x_dim))

        for t in range(self.seq_len):
            # For each time frame t, initialize the state vector as well as the variance matrix
            data_obs_t = data_obs[:, t, :, :]
            # if t == 0:
            if t < 30:
                # For the first frame, the positions are initialized as the first frame detections, the velocities are
                # initialized to 0, the variance matrix are initialized to a diagonal matrix plus a one matrix.
                x_mean_vem_t_init = torch.zeros(self.num_source, batch_size, self.x_dim).to(self.device)
                x_mean_vem_t_init[:, :, :self.o_dim] = data_obs_t.permute(1, 0, -1)
                x_var_vem_t_init = (0.0002 * torch.eye(self.x_dim, self.x_dim) + 0.001*torch.ones(self.x_dim, self.x_dim)).repeat(self.num_source, batch_size, 1, 1).to(self.device)
                Lambda_t_init = Lambda[:, :, t, :, :]
            else:
                num_block = t // 30
                x_mean_vem_t_init = torch.matmul(D, x_mean_vem_iter[-1, :, :, num_block*30-1, :].unsqueeze(-1)).squeeze()
                # For other frames, the state vector is initialized as Dm_{t-1}
                x_var_vem_t_init = (0.0002 * torch.eye(self.x_dim, self.x_dim) + 0.001*torch.ones(self.x_dim, self.x_dim)).repeat(self.num_source, batch_size, 1, 1).to(self.device)
                Lambda_t_init = Lambda[:, :, t, :, :]
            x_mean_vem_init[:, :, t, :] = x_mean_vem_t_init

            for i in range(self.N_iter):
                iter_start = datetime.datetime.now()
                ew_start = datetime.datetime.now()
                Eta_n_sum = torch.zeros(batch_size, self.num_obs).to(self.device)
                # E-W Step
                for n in range(self.num_source):
                    if i == 0:
                        x_mean_vem_t_n = x_mean_vem_t_init[n, :, :]
                        x_var_vem_t_n = x_var_vem_t_init[n, :, :, :]
                    else:
                        x_mean_vem_t_n = x_mean_vem_iter[i - 1, n, :, t, :]
                        x_var_vem_t_n = x_var_vem_iter[i - 1, n, :, t, :, :]
                    Phi_t = Phi[:, t, :, :, :]
                    Phi_inv_t = Phi_inv[:, t, :, :, :]
                    Eta_n = self.compute_eta_n(data_obs_t, A, Phi_t, Phi_inv_t, x_mean_vem_t_n, x_var_vem_t_n)
                    Eta_n_tosum = torch.clone(Eta_n)
                    Eta_n_tosum[torch.isnan(Eta_n_tosum)] = 0
                    Eta_n_sum += Eta_n_tosum
                    Eta_iter[i, n, :, t, :] = Eta_n
                Eta_n_sum = Eta_n_sum.expand(self.num_source, batch_size, self.num_obs)
                Eta_iter[i, :, :, t, :] = Eta_iter[i, :, :, t, :] / Eta_n_sum
                ew_end = datetime.datetime.now()
                ew_time = (ew_end - ew_start).seconds / 60
                print('E-W time {:.2f}m'.format(ew_time))
                # E-S Step
                es_start = datetime.datetime.now()
                for n in range(self.num_source):
                    if t == 0:
                        x_mean_vem_tm1_n = x_mean_vem_t_init[n, :, :]
                        x_var_vem_tm1_n = x_var_vem_t_init[n, :, :, :]
                    else:
                        x_mean_vem_tm1_n = x_mean_vem_iter[-1, n, :, t-1, :]
                        x_var_vem_tm1_n = x_var_vem_iter[-1, n, :, t-1, :, :]
                    if i == 0:
                        Lambda_tn = Lambda_t_init[n, :, :]
                    else:
                        Lambda_tn = Lambda_iter[i - 1, n, :, t, :, :]
                    eta_tn = Eta_iter[i, n, :, t, :]
                    phi_inv_t = Phi_inv[:, t, :, :, :]
                    x_var_vem_t_n = self.compute_x_var_vem_nt(x_var_vem_tm1_n, eta_tn, A, D, Lambda_tn, phi_inv_t)
                    x_mean_vem_t_n = self.compute_x_mean_vem_nt(x_mean_vem_tm1_n, data_obs_t, x_var_vem_t_n, x_var_vem_tm1_n, eta_tn, A,
                                          D, Lambda_tn, phi_inv_t)
                    x_mean_vem_iter[i, n, :, t, :] = x_mean_vem_t_n
                    x_var_vem_iter[i, n, :, t, :, :] = x_var_vem_t_n
                es_end = datetime.datetime.now()
                es_time = (es_end - es_start).seconds / 60
                print('E-S time {:.2f}m'.format(es_time))
                # M Step
                em_start = datetime.datetime.now()
                if t == 0:
                    x_mean_vem_tm1 = x_mean_vem_t_init
                    x_var_vem_tm1 = x_var_vem_t_init
                else:
                    x_mean_vem_tm1 = x_mean_vem_iter[-1, :, :, t-1, :]
                    x_var_vem_tm1 = x_var_vem_iter[-1, :, :, t - 1, :, :]
                x_mean_vem_t = x_mean_vem_iter[i, :, :, t, :]
                x_var_vem_t = x_var_vem_iter[i, :, :, t, :, :]
                Lambda_t = self.compute_lambda_t(x_mean_vem_t, x_mean_vem_tm1, x_var_vem_t, x_var_vem_tm1, D)
                Lambda_iter[i, :, :, t, :, :] = Lambda_t
                em_end = datetime.datetime.now()
                em_time = (em_end - em_start).seconds / 60
                print('E-M time {:.2f}m'.format(em_time))

                iter_end = datetime.datetime.now()
                iter_time = (iter_end - iter_start).seconds / 60
                print('Iter time {:.2f}m'.format(iter_time))

        result_list = [Eta_iter, x_mean_vem_iter, x_var_vem_iter, Lambda_iter, data_obs, data_gt]
        if batch_idx % save_frequence == 0:
            self.save_log.save_KF_results(batch_idx, result_list)

        return Eta_iter, x_mean_vem_iter, x_var_vem_iter, Lambda_iter

    def compute_eta_n(self, o, A, Phi, Phi_inv, x_mean_vem_n, x_var_vem_n):
        ### Tensor dimensions
        # o: (batch_size, num_obs, o_dim)
        # A: (o_dim, x_dim)
        # Phi: (batch_size, num_obs, o_dim, o_dim)
        # Phi_inv: (batch_size, num_obs, o_dim, o_dim)
        # x_mean_vem_n: (batch_size, x_dim)
        # x_var_vem_n: (batch_size, x_dim, x_dim)

        # Eta_n: (batch_size, num_obs)
        batch_size = o.shape[0]

        Eta_n = torch.zeros(self.num_obs, batch_size).to(self.device)

        for k in range(self.num_obs):
            Phi_k = Phi[:, k, :, :]
            Phi_inv_k = Phi_inv[:, k, :, :]
            o_k = o[:, k, :]

            det_Phi_k = torch.det(Phi_k)
            det_Phi_k_sqrt = torch.sqrt(det_Phi_k)

            A_ms = torch.matmul(A, x_mean_vem_n.unsqueeze(-1))
            o_A_ms = o_k.unsqueeze(-1) - A_ms
            o_A_ms_Phi = torch.matmul(o_A_ms.squeeze().unsqueeze(-2), Phi_inv_k)
            o_A_ms_Phi_sq = torch.matmul(o_A_ms_Phi, o_A_ms)
            o_A_ms_Phi_sq = 0.3*o_A_ms_Phi_sq.squeeze()
            gaussian_exp_term = torch.exp(-0.5*o_A_ms_Phi_sq)

            A_Phi = torch.matmul(torch.transpose(A, 0, 1), Phi_inv_k)
            A_Phi_A = torch.matmul(A_Phi, A)
            A_Phi_A_Sigma = torch.matmul(A_Phi_A, x_var_vem_n)
            trace = 0.1*torch.diagonal(A_Phi_A_Sigma, dim1=-2, dim2=-1).sum(-1)
            exp_tr_term = torch.exp(-0.5 * trace)

            Beta_n = (1 / det_Phi_k_sqrt) * gaussian_exp_term * exp_tr_term + 0.0000000001
            Eta_n[k, :] = Beta_n / (self.num_source + 1)

        Eta_n = Eta_n.permute(1, 0)

        return Eta_n

    def compute_x_var_vem_nt(self, x_var_vem_tm1_n, eta_tn, A, D, Lambda_tn, phi_inv_t):
        batch_size = eta_tn.shape[0]
        num_obs = eta_tn.shape[1]
        term_obs = torch.zeros(batch_size, self.x_dim, self.x_dim).to(self.device)
        for k in range(num_obs):
            Phi_inv_tk = phi_inv_t.permute(1, 0, 2, 3)[k, :, :, :]
            Eta_tkn = eta_tn.permute(1, 0)[k]
            A_Phi = torch.matmul(torch.transpose(A, 0, 1), Phi_inv_tk)
            A_Phi_A = torch.matmul(A_Phi, A)
            Eta_A_Phi_A = (Eta_tkn.unsqueeze(-1) * A_Phi_A.view(batch_size, self.x_dim * self.x_dim)).view(batch_size,
                                                                                                         self.x_dim,
                                                                                                         self.x_dim)
            Eta_A_Phi_A[torch.isnan(Eta_A_Phi_A)] = 0
            term_obs += Eta_A_Phi_A

        term_dynamic = Lambda_tn + 0.0001 * torch.eye(self.x_dim).to(self.device)
        term_dynamic_inv = torch.zeros(batch_size, self.x_dim, self.x_dim)
        try:
            u = torch.cholesky(term_dynamic)
            for i in range(batch_size):
                term_dynamic_inv[i, :, :] = torch.cholesky_inverse(u[i, :, :])
        except RuntimeError:
            print(term_dynamic)
            import pdb; pdb.set_trace()

        term_sum = term_obs + term_dynamic_inv

        x_var_vem_t_n = torch.zeros(batch_size, self.x_dim, self.x_dim)
        try:
            u = torch.cholesky(term_sum)
        except RuntimeError:
            print(term_sum)
        for i in range(batch_size):
            x_var_vem_t_n[i, :, :] = torch.cholesky_inverse(u[i, :, :])

        return x_var_vem_t_n

    def compute_x_mean_vem_nt(self, x_mean_vem_tm1_n, data_obs_t, x_var_vem_t_n, x_var_vem_tm1_n, eta_tn, A, D, Lambda_tn, phi_inv_t):
        batch_size = eta_tn.shape[0]
        num_obs = eta_tn.shape[1]

        term_obs = torch.zeros(batch_size, self.x_dim).to(self.device)
        for k in range(num_obs):
            Phi_inv_tk = phi_inv_t.permute(1, 0, 2, 3)[k, :, :, :]
            Eta_tkn = eta_tn.permute(1, 0)[k]
            o_tk = data_obs_t.permute(1, 0, 2)[k, :, :]
            A_Phi = torch.matmul(torch.transpose(A, 0, 1), Phi_inv_tk)
            A_Phi_o = torch.matmul(A_Phi, o_tk.unsqueeze(-1))
            Eta_A_Phi_o = Eta_tkn.unsqueeze(-1) * A_Phi_o.squeeze()
            Eta_A_Phi_o[torch.isnan(Eta_A_Phi_o)] = 0
            term_obs += Eta_A_Phi_o

        var_term_dynamic = Lambda_tn + 0.0001 * torch.eye(self.x_dim).to(self.device)
        var_term_dynamic_inv = torch.zeros(batch_size, self.x_dim, self.x_dim)
        try:
            u = torch.cholesky(var_term_dynamic)
        except RuntimeError:
            print(var_term_dynamic)
        for i in range(batch_size):
            var_term_dynamic_inv[i, :, :] = torch.cholesky_inverse(u[i, :, :])
        var_D = torch.matmul(var_term_dynamic_inv, D)
        term_dynamic = torch.matmul(var_D, x_mean_vem_tm1_n.unsqueeze(-1)).squeeze()

        term_sum = term_obs + term_dynamic

        x_mean_vem_t_n = torch.matmul(x_var_vem_t_n, term_sum.unsqueeze(-1)).squeeze()

        return x_mean_vem_t_n

    def compute_lambda_t(self, x_mean_vem_t, x_mean_vem_tm1, x_var_vem_t, x_var_vem_tm1, D):
        D_Gamma = torch.matmul(D, x_var_vem_tm1)
        D_Gamma_D = torch.matmul(D_Gamma, torch.transpose(D, 0, 1))
        term_var = x_var_vem_t - D_Gamma_D

        D_mu = torch.matmul(D, x_mean_vem_tm1.unsqueeze(-1)).squeeze()
        mu_D_mu = x_mean_vem_t - D_mu
        term_mean = torch.matmul(mu_D_mu.unsqueeze(-1), torch.transpose(mu_D_mu.unsqueeze(-1), 2, 3))

        Lambda_t = x_var_vem_t + term_mean

        return Lambda_t











